if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

CYAN="\[$(tput setaf 6)\]"
GREEN="\[$(tput setaf 2)\]"
PURPLE="\[$(tput setaf 5)\]"
RESET="\[$(tput sgr0)\]"

PS1="${PURPLE}[${GREEN}\u@\h${RESET} ${CYAN}\W${PURPLE}]${RESET}\$ "
umask 077

export HISTSIZE=5000
export LESSHISTFILE="/dev/null"
export PYTHONSTARTUP="${HOME}/.pythonrc"


alias curl="/usr/bin/curl --verbose --user-agent 'Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0'"
alias scurl2="/usr/bin/curl --verbose --user-agent 'Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0' --tlsv1.2 --proto =https"
alias scurl3="/usr/bin/curl --verbose --user-agent 'Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0' --tlsv1.3 --proto =https"
alias tscurl2="/usr/bin/torsocks /usr/bin/curl --verbose --user-agent 'Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0' --tlsv1.2 --proto =https"
alias tscurl3="/usr/bin/torsocks /usr/bin/curl --verbose --user-agent 'Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0' --tlsv1.3 --proto =https"
alias diff="diff --color=auto"
alias grep="grep --color=auto"
alias ls="ls -lhF --color=auto"
alias sway="WLR_NO_HARDWARE_CURSORS=1 sway"
alias su="/usr/bin/su --login"


# Convenient file encryption using GPG:
encrypt-file() {
    /usr/bin/gpg --verbose --no-symkey-cache --cipher-algo AES-256  \
        --output "${2}" --symmetric "${1}"
}
