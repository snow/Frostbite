let skip_defaults_vim=1
set noexrc
set nocompatible
set secure
set encoding=utf8
set fileencoding=utf8

set viminfo=""
set noswapfile
let g:netrw_dirhistmax=0

set cursorline
set number
set lazyredraw
set ttyfast
filetype plugin on

syntax on
set t_Co=256
set colorcolumn=121
highlight ColorColumn ctermbg=darkgray
au BufNewFile,BufFilePre,BufRead *.log set filetype=conf
au BufNewFile,BufFilePre,BufRead *.txt set filetype=conf
au BufNewFile,BufFilePre,BufRead .* set filetype=conf

set tabstop=4
set shiftwidth=4
set softtabstop=0
set expandtab
set smartindent

set ignorecase
set incsearch
set hlsearch

set pastetoggle=<F4>
