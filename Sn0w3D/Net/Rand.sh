#!/bin/bash

#   .___.__
#  __| _/|  |__ ___  _________
# / __ | |  |  \\  \/  /\____ \
#/ /_/ | |   Y  \>    < |  |_> >
#\____ | |___|  /__/\_ \|   __/
#     \/      \/      \/|__|

if [[ "$(id -u)" -ne 0 ]]; then
    echo "[!] Run Me As Root [!]"
    exit 1
fi

sudo ip link set eth0 up
sudo ip addr add 10.37.24.1/24 broadcast 10.37.24.255 dev eth0
sudo ip route add default via 10.37.24.1

sudo cp -r /home/user/Net/resolv.conf /etc/resolv.conf
sudo arp -s 10.37.24.1 B8:27:EB:34:25:DD
sleep 2