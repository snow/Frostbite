if [[ "$(id -u)" -ne 0 ]]; then
    echo "[!] Run Me As Root !"
    exit 1
fi

umask 077

#Set The Username
echo -n "[?] Whats Your Username [?]"
read username

sleep 2

echo -n "Is This The Correct Username "${username}"? [Y/N] > "
read failsafe
if [[ "$failsafe" = Y ]]; then
	echo "[*] Installing Hardened-Kernel"
	echo " "
	sleep 5

    COMP_JOBS=6
    # Build and install a minimally bloated, maximally hardened configuration of "linux-hardened":
        HARDENED_KVER="5.15.25"
        HARDENED_KNAME="${HARDENED_KVER}-hardened1"
        ## Install packages required for compiling the "linux-hardened" linux kernel:
            pacman -Sy bc bison flex
        ## Obtain the selected release of linux-hardened:
            cd /usr/src/
            mkdir ./linux-hardened/
            chmod 0755  ./linux-hardened/
            cd ./linux-hardened/
            /usr/bin/curl --verbose --tlsv1.3 --proto =https -L -O --url "https://github.com/anthraxx/linux-hardened/archive/refs/tags/${HARDENED_KNAME}.tar.gz"
            tar --verbose --extract --file ./"${HARDENED_KNAME}".tar.gz
            mv ./linux-hardened-"${HARDENED_KNAME}"/    ./"${HARDENED_KNAME}"
            chmod 0755  ./"${HARDENED_KNAME}"/
        ## Compile and install linux-hardened using a minimal configuration with additional security enhancements:
            #cd ./"${HARDENED_KNAME}"/
            cd /home/"${username}"/Sn0w3D/
            cat _.config   > ./.config
            umask 022
            #   ^ Temporarily set a more relaxed umask to prevent future problems that can occur when building out-of-tree
            #       kernel modules.
            make --jobs="${COMP_JOBS}"
            make modules_install
            umask 077
            #   ^ Revert the temporary umask change.
            cp ./arch/x86_64/boot/bzImage /boot/vmlinuz-"${HARDENED_KNAME}"
            mkinitcpio --kernel /boot/vmlinuz-"${HARDENED_KNAME}" --generate /boot/initramfs-"${HARDENED_KNAME}".img || /usr/bin/true
            #   ^ All "errors" are ignored to prevent non-issues from stopping the script.
            sed -i 's/GRUB_DEFAULT=0/GRUB_DEFAULT=2/g'  /etc/default/grub
            #   ^ Sets the default boot option to the third menu entry in GRUB which should be linux-hardened, the count
            #       starts from zero.
            grub-mkconfig --output=/boot/grub/grub.cfg
            #   make clean
            #   ^ Don't clean the build directory for linux-hardened yet as we need to use it for building+signing LKRG.

        # Implement LKRG to run alongside linux and linux-hardened:
        LKRG_VER="0.9.2"
        ## Install packages required to support using LKRG with vanilla linux:
            pacman -S linux-headers
        ## Obtain the selected release of LKRG and files used for integrity verification:
            cd /usr/src/
            mkdir ./LKRG/
            cd ./LKRG
            /usr/bin/curl --verbose --tlsv1.3 --proto =https -O --url "https://lkrg.org/download/lkrg-${LKRG_VER}.tar.gz"
            /usr/bin/curl --verbose --tlsv1.3 --proto =https -O --url "https://lkrg.org/download/lkrg-${LKRG_VER}.tar.gz.sign"
            /usr/bin/curl --verbose --tlsv1.3 --proto =https -O --url "https://www.openwall.com/signatures/openwall-offline-signatures.asc"
        ## Verify the integrity of LKRG:
            gpg --import ./openwall-offline-signatures.asc
            gpg --verify lkrg-"${LKRG_VER}".tar.gz.sign lkrg-"${LKRG_VER}".tar.gz
        ## Compile and install LKRG with a hardened configuration:
            tar --verbose --extract --file ./lkrg-"${LKRG_VER}".tar.gz
            cd ./lkrg-"${LKRG_VER}"/
            # Build LKRG for linux-hardened:
            make --jobs="${COMP_JOBS}"  KERNELRELEASE="${HARDENED_KNAME}"
            make install                KERNELRELEASE="${HARDENED_KNAME}"
            make clean                  KERNELRELEASE="${HARDENED_KNAME}"
            # Build LKRG for vanilla linux:
            make --jobs="${COMP_JOBS}"
            make install
            make clean
            # Ensure the LKRG module is loaded at boot and install relevant sysctl settings:
            cd /home/"${username}"/Sn0w3D/
            echo    '/usr/bin/modprobe p_lkrg'                      >>  /etc/rc.local
            cat 01-lkrg.conf            >   /etc/sysctl.d/01-lkrg.conf
        ## Cleanup the build directory for linux-hardened now that it's no longer needed:
            cd /usr/src/linux-hardened/"${HARDENED_KNAME}"/
            make clean