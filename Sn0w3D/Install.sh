#!/bin/bash

#Reverse Indenting V1.0

set -e
set -o pipefail

if [[ "$(id -u)" -ne 0 ]]; then
    echo "[!] Run Me As Root !"
    exit 1
fi

clear

echo "           RIP dud1337 - You Will Be Missed                "
echo "                                                           "
echo "This Script Is A Combination Of DriftNotSkids PARSEC Script"
echo "And Is Also Derived From Maidaidans Hardening Guide"
echo "It Is Created By Sn0w . . . yea I know right? that cunt really?"
echo "[!] Read 'READMESNOW' Before You Execute This Script [!]"
echo "[?] Install Sn0w's Custom Hardening Script [?]"

echo -n "Execute? [Y/N] > "
read execute
if [[ "$execute" = Y ]]; then
	echo "[*] Installing Sn0ws Configuration"
	echo " "
	sleep 5

	#Set The Username
	echo -n "[?] Whats Your Username [?]"
	read username

	#Set A Secure Umask For The Script Session
	umask 077

	#Enable Archlinux Repos
	cd /home/"${username}"/Sn0w3D/
	pacman -Sy artix-archlinux-support
	cp -r pacman.conf  > /etc/pacman.conf
    pacman-key --populate archlinux
    pacman -Syu

	#PAM
	cd /home/"${username}"/Sn0w3D/
	gpasswd --add root wheel
	pacman -Syu libpwquality
	cat login.defs                                   > /etc/login.defs
    cat securetty                                    > /etc/securetty
    cat security/access.conf                         > /etc/security/access.conf
	chmod 0600  /etc/security/access.conf
    mkdir /usr/lib/whonix-ports/
    chmod 0755  /usr/lib/whonix-ports/
	cat whonix-ports/pam-restrict-services.sh    > /usr/lib/whonix-ports/pam-restrict-services.sh
	chmod 0700  /usr/lib/whonix-ports/pam-restrict-services.sh
	chmod 0700 /etc/pam.d/

    cat /pam/pam.d/chage                                  > /etc/pam.d/chage
    cat /pam/pam.d/chfn                                   > /etc/pam.d/chfn
    cat /pam/pam.d/chgpasswd                              > /etc/pam.d/chgpasswd
    cat /pam/pam.d/chpasswd                               > /etc/pam.d/chpasswd
    cat /pam/pam.d/chsh                                   > /etc/pam.d/chsh
    cat /pam/pam.d/doas                                   > /etc/pam.d/doas
    cat /pam/pam.d/elogind-user                           > /etc/pam.d/elogind-user
    cat /pam/pam.d/groupadd                               > /etc/pam.d/groupadd
    cat /pam/pam.d/groupdel                               > /etc/pam.d/groupdel
    cat /pam/pam.d/groupmems                              > /etc/pam.d/groupmems
    cat /pam/pam.d/groupmod                               > /etc/pam.d/groupmod
    cat /pam/pam.d/login                                  > /etc/pam.d/login
    cat /pam/pam.d/newusers                               > /etc/pam.d/newusers
    cat /pam/pam.d/other                                  > /etc/pam.d/other
    cat /pam/pam.d/passwd                                 > /etc/pam.d/passwd
    cat /pam/pam.d/password-auth                          > /etc/pam.d/password-auth
    cat /pam/pam.d/postlogin                              > /etc/pam.d/postlogin
    cat /pam/pam.d/remote                                 > /etc/pam.d/remote
    cat /pam/pam.d/runuser                                > /etc/pam.d/runuser
    cat /pam/pam.d/runuser-l                              > /etc/pam.d/runuser-l
    cat /pam/pam.d/shadow                                 > /etc/pam.d/shadow
    cat /pam/pam.d/su                                     > /etc/pam.d/su
    cat /pam/pam.d/su-l                                   > /etc/pam.d/su-l
    cat /pam/pam.d/system-auth                            > /etc/pam.d/system-auth
    cat /pam/pam.d/system-local-login                     > /etc/pam.d/system-local-login
    cat /pam/pam.d/system-login                           > /etc/pam.d/system-login
    cat /pam/pam.d/system-remote-login                    > /etc/pam.d/system-remote-login
    cat /pam/pam.d/system-services                        > /etc/pam.d/system-services
    cat /pam/pam.d/useradd                                > /etc/pam.d/useradd
    cat /pam/pam.d/userdel                                > /etc/pam.d/userdel
    cat /pam/pam.d/usermod                                > /etc/pam.d/usermod
    cat /pam/pam.d/util-auth                              > /etc/pam.d/util-auth
    cat /pam/pam.d/vlock                                  > /etc/pam.d/vlock
    chmod 0600 /etc/pam.d/*

	#Install Dependencies
	pacman -Syu gcc make tor tor-runit unzip torsocks opendoas bubblewrap-suid libpwquality

	#Hardens Kernel
	echo "[!] Hardening Kernel"
	cd /home/"${username}"/Sn0w3D/
	cp DragonBall.conf /etc/sysctl.d/
	cp 01-lkrg.conf /etc/sysctl.d/
	sleep 5

	#Installs App Armour
	echo "[*] Installing App Armour"
	cd /home/"${username}"/Sn0w3D/
	pacman -Syu --noconfirm apparmor apparmor-runit
	echo 'GRUB_CMDLINE_LINUX_DEFAULT+=" apparmor=1 security=apparmor"' >> /etc/default/grub
	cp -r apparmor.conf /etc/rc/apparmor.conf

	update-grub
	sleep 5

	#Install Macchanger
	sudo pacman -Syu --noconfirm macchanger
	echo    'ip link set eth0 down && macchanger -r eth0 && ip link set eth0 up > /dev/null'   >> /etc/rc.local
	sleep 5

	# Disable core dumps:
	cd /home/"${username}"/Sn0w3D/
	mkdir /etc/security/limits.d/
    chmod 0755  /etc/security/limits.d/
	cp -r coredumps.conf /etc/security/limits.d/coredumps.conf
	chmod 0644  /etc/security/limits.d/coredumps.conf

	# Create system groups that can be used to bypass restrictions applied by the "hide-hardware-info" service:
    groupadd --system cpuinfo
    groupadd --system sysfs

	#Simple Whonix-Permissions-Hardening
	cd /home/"${username}"/Sn0w3D/
	cat whonix-ports/permission-hardening.sh         > /usr/lib/whonix-ports/permission-hardening.sh
    chmod 0700 whonix-ports/permission-hardening.sh
    mkdir /etc/permission-hardening.d/
    chmod 0700 /etc/permission-hardening.d/
    cat whonix-ports/permission-hardening.d/20-basic_dac_perms.conf   > /etc/permission-hardening.d/20-basic_dac_perms.conf
    cat whonix-ports/permission-hardening.d/20-setid_stripper.conf    > /etc/permission-hardening.d/20-setid_stripper.conf
    chmod 0700  whonix-ports/permission-hardening.d/*.conf
    echo    '/usr/lib/whonix-ports/permission-hardening.sh > /dev/null'     >> /etc/rc.local
    /usr/lib/whonix-ports/permission-hardening.sh

	#Whonix Hide-Hardware-Info
	cd /home/"${username}"/Sn0w3D/
	cat whonix-ports/hide-hardware-info.sh           > /usr/lib/whonix-ports/hide-hardware-info.sh
    chmod 0700  /usr/lib/whonix-ports/hide-hardware-info.sh
    mkdir /etc/hide-hardware-info.d/
    cat whonix-ports/hide-hardware-info.d/30-defaults.conf            > /etc/hide-hardware-info.d/30-defaults.conf
    echo    '/usr/lib/whonix-ports/hide-hardware-info.sh > /dev/null 2>&1'  >> /etc/rc.local
    /usr/lib/whonix-ports/hide-hardware-info.sh

	# Implement the "System.map" file removal service from Whonix:
    cd /home/"${username}"/Sn0w3D/
	cat whonix-ports/remove-system-map.sh        > /usr/lib/whonix-ports/remove-system-map.sh
    chmod 0700  /usr/lib/whonix-ports/remove-system-map.sh
    echo    '/usr/lib/whonix-ports/remove-system-map.sh > /dev/null'    >> /etc/rc.local
    /usr/lib/whonix-ports/remove-system-map.sh

	# Implement the "bootclockrandomization" service from Whonix:
	cd /home/"${username}"/Sn0w3D/
    mkdir /usr/lib/whonix-ports/bootclockrandomization/
    chmod 0700 whonix-ports/bootclockrandomization/
    cat whonix-ports/bootclockrandomization/shared.sh                > /usr/lib/whonix-ports/bootclockrandomization/shared.sh
    cat whonix-ports/bootclockrandomization/start.sh                 > /usr/lib/whonix-ports/bootclockrandomization/start.sh
    cat whonix-ports/bootclockrandomization/stop.sh                  > /usr/lib/whonix-ports/bootclockrandomization/stop.sh
    chmod 0700  /usr/lib/whonix-ports/bootclockrandomization/*.sh
    cat whonix-ports/bootclockrandomization/tmpfiles.d/bootclockrandomization.conf                           > /etc/tmpfiles.d/bootclockrandomization.conf
    chmod 0644  /etc/tmpfiles.d/bootclockrandomization.conf
    tmpfiles --create /etc/tmpfiles.d/bootclockrandomization.conf
    echo    '/usr/lib/whonix-ports/bootclockrandomization/start.sh > /dev/null       &&  \' >> /etc/rc.local
    echo    '    /usr/lib/whonix-ports/bootclockrandomization/stop.sh  > /dev/null'         >> /etc/rc.local
    /usr/lib/whonix-ports/bootclockrandomization/start.sh   && \
        /usr/lib/whonix-ports/bootclockrandomization/stop.sh

	#Account Hardening
	#Root
	cd /home/"${username}"/Sn0w3D/
	cat accounts/root/_.bash_logout           > /root/.bash_logout
    cat accounts/root/_.bash_profile          > /root/.bash_profile
    cat accounts/root/_.bashrc                > /root/.bashrc
    chmod 0600  /root/.bash*
    mkdir --parents /root/.config/procps/
    cat accounts/root/_.config/procps/toprc   > /root/.config/procps/toprc
    cat accounts/root/_.pythonrc              > /root/.pythonrc
    cat accounts/root/_.vimrc                 > /root/.vimr
	#Admin "mrfbiman"
	cat accounts/home/mrfbiman/_.bash_logout         > /home/mrfbiman/.bash_logout
	cat accounts/home/mrfbiman/_.bash_profile        > /home/mrfbiman/.bash_profile
    cat accounts/home/mrfbiman/_.bashrc              > /home/mrfbiman/.bashrc
    chmod 0600  /home/mrfbiman/.bash*
    mkdir --parents /home/mrfbiman/.config/procps/
    cat accounts/home/mrfbiman/_.config/procps/toprc > /home/mrfbiman/.config/procps/toprc
    cat accounts/home/mrfbiman/_.pythonrc            > /home/mrfbiman/.pythonrc
    cat accounts/home/mrfbiman/_.vimrc               > /home/mrfbiman/.vimrc
    chown --recursive mrfbiman:mrfbiman   /home/mrfbiman/
    #Snow
    cat accounts/home/snow/_.bash_logout          > /home/snow/.bash_logout
    cat accounts/home/snow/_.bash_profile         > /home/snow/.bash_profile
    cat accounts/home/snow/_.bashrc               > /home/snow/.bashrc
    chmod 0600  /home/snow/.bash*
    mkdir --parents /home/snow/.config/procps/
    cat accounts/home/snow/_.config/procps/toprc  > /home/snow/.config/procps/toprc
    cat accounts/home/snow/_.pythonrc             > /home/snow/.pythonrc
    cat accounts/home/snow/_.vimrc                > /home/snow/.vimrc
    chown --recursive snow:snow /home/snow/

	#Harden More Shit
    groupadd --system procfs

	cd /home/"${username}"/Sn0w3D/
    OLD_LUKS_UUID="$(grep -E -o "[0-9A-Za-z]+-[0-9A-Za-z]+-[0-9A-Za-z]+-[0-9A-Za-z]+-[0-9A-Za-z]+:crypt0" grub  |  cut -d ":" -f 1)"
    FIRST_LUKS_DEV_UUID="$(lsblk --list --paths --output FSTYPE,UUID  |  grep "crypto_LUKS" -m 1  |  cut -d " " -f 2)"
    ADMIN_UID="$(id --user "${username}")"
    USER_UID="$(id --user snow)"
    PROCFS_GID="$(grep procfs /etc/group  |  cut -d ":" -f 3)"
    OLD_ADMIN_FSTAB="$(grep -E "/home/"${username}"/.cache .*,uid=[0-9]+,gid=[0-9]+"  fstab)"
    ESCAPED_ADMIN_FSTAB="$(echo "${OLD_ADMIN_FSTAB}"  |  sed 's/\//\\\//g')"
    NEW_ADMIN_FSTAB="$(echo "${ESCAPED_ADMIN_FSTAB}"  |  sed -E "s/,uid=[0-9]+,gid=[0-9]+ /,uid=${ADMIN_UID},gid=${ADMIN_UID} /")"
    OLD_USER_FSTAB="$(grep -E "/home/"${username}"/.cache .*,uid=[0-9]+,gid=[0-9]+"  fstab)"
    ESCAPED_USER_FSTAB="$(echo "${OLD_USER_FSTAB}"  |  sed 's/\//\\\//g')"
    NEW_USER_FSTAB="$(echo "${ESCAPED_USER_FSTAB}"  |  sed -E "s/,uid=[0-9]+,gid=[0-9]+ /,uid=${USER_UID},gid=${USER_UID} /")"
    OLD_PROCFS_FSTAB="$(grep -E "/proc .*,gid=[0-9]+"  fstab)"
    ESCAPED_PROCFS_FSTAB="$(echo "${OLD_PROCFS_FSTAB}"  |  sed 's/\//\\\//g')"
    NEW_PROCFS_FSTAB="$(echo "${ESCAPED_PROCFS_FSTAB}"  |  sed -E "s/,gid=[0-9]+ /,gid=${PROCFS_GID} /")"

	sed -i "s/${OLD_LUKS_UUID}/${FIRST_LUKS_DEV_UUID}/" /etc/default/grub
    grub-mkconfig --output=/boot/grub/grub.cfg

	cd /home/"${username}"/Sn0w3D/
	cat fstab > /etc/fstab
    sed -i "s/${ESCAPED_ADMIN_FSTAB}/${NEW_ADMIN_FSTAB}/"   /etc/fstab
    sed -i "s/${ESCAPED_USER_FSTAB}/${NEW_USER_FSTAB}/"     /etc/fstab
    sed -i "s/${ESCAPED_PROCFS_FSTAB}/${NEW_PROCFS_FSTAB}/" /etc/fstab
    #Make Sure Proc Hardening Options FUCKING Work ,._.,
	cd /home/"${username}"/Sn0w3D/
    cat rc.local > /etc/rc.local

	#Make .cache utilize TMPFS mount configurations
	#snow
	rm -rf  /snow/.cache/
    mkdir   /snow/.cache/
    chmod 0000  /snow/.cache/
    chattr +i   /snow/.cache/
	#root
	rm -rf  /root/.cache/
    mkdir   /root/.cache/
    chmod 0000  /root/.cache/
    chattr +i   /root/.cache/
	#mrfbiman
	rm -rf  /mrfbiman/.cache/
    mkdir   /mrfbiman/.cache/
    chmod 0000  /mrfbiman/.cache/
    chattr +i   /mrfbiman/.cache/
	
	#Permission Hardening
	chmod 0700 /boot /usr/src /lib/modules /usr/lib/modules
	
	#Use Generic Whonix Machine-ID
	cd /home/"${username}"/Sn0w3D
	cp -r machine-id /etc/
	
	#Install Kloak
	echo "[*] Install Kloak"
	cd /home/"${username}"/Sn0w3D/
	unzip kloak.zip
	cd kloak
	make all
	cp /home/"${username}"/Sn0w3D/kloak/kloak     /usr/sbin/kloak
    cp /home/"${username}"/Sn0w3D/kloak/eventcap  /usr/sbin/eventcap
    make clean
	chmod 0755  /etc/runit/sv/kloak/
	chmod 0755  /etc/runit/sv/kloak/run
	sleep 5

	#Install Hardened Malloc
	echo "[*] Install Hardened Malloc"
	cd /home/"${username}"/Sn0w3D/
	unzip hardened_malloc.zip
	chmod 0755 hardened_malloc
	cd hardened_malloc
	make
	mkdir -p /usr/lib/libhardened_malloc.so
	cp libhardened_malloc.so /usr/lib/libhardened_malloc.so/libhardened_malloc.so
	make clean
	chmod 0755 /usr/lib/libhardened_malloc.so/libhardened_malloc.so
	make CONFIG_GUARD_SLABS_INTERVAL=8 CONFIG_SLAB_QUARANTINE_QUEUE_LENGTH=0 CONFIG_SLAB_QUARANTINE_RANDOM_LENGTH=0
	cd /home/"${username}"/Sn0w3D/
	cp -r ld.so.preload /etc/
	chmod 0644 /etc/ld.so.preload
	sleep 5 
	
	#Firewall
	pacman -Syu --noconfirm iptables iptables-runit
	cd /home/"${username}"/Sn0w3D
	cp -r iptables.rules /etc/iptables/
	chmod 0600  /etc/iptables/iptables.rules

	sleep 5
	
	#Doas
	cd /home/"${username}"/Sn0w3D
	cp -r doas.conf /etc/
	
	#Harden Grub
	echo "[*] Harden Grub"
	update-grub
	sleep 5

	#Install And Configure A Hardened Tor Daemon
	cd /home/"${username}"/Sn0w3D
	cp -r torrc > /etc/tor/torrc
    chmod 0600  /etc/tor/torrc

	# Install and configure dnscrypt-proxy for privacy-friendly DNS functionality
	cd /home/"${username}"/Sn0w3D/
	pacman -Sy dnscrypt-proxy dnscrypt-proxy-runit
	useradd --no-create-home --system --user-group dnscrypt
    cat dnscrypt-proxy.toml   > /etc/dnscrypt-proxy/dnscrypt-proxy.toml
	cat resolv.conf  > /etc/resolv.conf
    chmod 0444  /etc/resolv.conf
    chattr +i   /etc/resolv.conf

	#Blacklist Modules With ModProbe
	echo "[*] Blacklist Modules With ModProbe"
	sleep 2
	cd /home/"${username}"/Sn0w3D/
	cp -r blacklist.conf /etc/modprobe.d/

	#Make Kloak And Other Services Run On Boot VIA Runit
	cd /home/"${username}"/Sn0w3D/runit
	cp -r Kloak /etc/runit/sv
	cp -r SecureTimeSync /etc/runit/sv
	cp -r Hide-Hardware-Info /etc/runit/sv
	cp -r ByeLogs /etc/runit/sv
	ln -s /etc/runit/sv/iptables/ /run/runit/service/iptables
	ln -s /etc/runit/sv/Kloak /run/runit/service/Kloak
	ln -s /etc/runit/sv/SecureTimeSync /run/runit/service/SecureTimeSync
	ln -s /etc/runit/sv/Hide-Hardware-Info /run/runit/service/Hide-Hardware-Info
	ln -s /etc/runit/sv/ByeLogs /run/runit/service/ByeLogs
	ln -s /etc/runit/sv/tor/ /run/runit/service/
	ln -s /etc/runit/sv/dnscrypt-proxy/ /run/runit/service/

	#Fuck NTP
	echo "[*] Fuck NTP Lets Get That Bitch Out (Secure How The System Gets Time)"
	cd /home/"${username}"/Sn0w3D/secure-time-sync
	chmod 0755  /etc/runit/sv/SecureTimeSync/
	chmod 0755  /etc/runit/sv/SecureTimeSync/run
	chmod 0700 secure-time-sync.sh
	bash secure-time-sync.sh

	#Runit Perms
	chmod 0755  /etc/runit/sv/Kloak/run

	sleep 5

	#Install CPU Microcode updates (Intel)
	pacman -Syu --noconfirm intel-ucode

	#Start Runit Services
	sv up iptables
	sv up Kloak
	sv up SecureTimeSync
	sv up Hide-Hardware-Info
	sv up ByeLogs
	sv up tor
	sv up dnscrypt-proxy

	#Update ManDB
	mandb

	#Show Tips
	sleep 5
	clear
	echo "  [!] Make Sure To Reboot Your System & Once Its Rebooted Run Hardened-Kernel.sh [!]                                  "
	echo " "
 	echo "[?] Your System Is Now Hardened But Dont Assume Your Invincible Because Of A Script [!]"
	echo "Read The System Hardening Checklist http://www.dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/wiki/System_Hardening_Checklist#Introduction"
	echo "Read The Rest Of Madaidans Guide > https://madaidans-insecurities.github.io/guides/linux-hardening.html"
	echo "Read Up On The Whonix Wiki > http://www.dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/wiki/Documentation" 
fi
#                                   M
#                                  dM
#                                  MMr
#                                 4MMML                  .
#                                 MMMMM.                xf
#                 .              "MMMMM               .MM-
#                  Mh..          +MMMMMM            .MMMM
#                  .MMM.         .MMMMML.          MMMMMh
#                   )MMMh.        MMMMMM         MMMMMMM
#                    3MMMMx.     'MMMMMMf      xnMMMMMM"
#                    '*MMMMM      MMMMMM.     nMMMMMMP"
#                      *MMMMMx    "MMMMM\    .MMMMMMM=
#                       *MMMMMh   "MMMMM"   JMMMMMMP
#                         MMMMMM   3MMMM.  dMMMMMM            .
#                          MMMMMM  "MMMM  .MMMMM(        .nnMP"
#              =..          *MMMMx  MMM"  dMMMM"    .nnMMMMM*
#                "MMn...     'MMMMr 'MM   MMM"   .nMMMMMMM*"
#                 "4MMMMnn..   *MMM  MM  MMP"  .dMMMMMMM""
#                   ^MMMMMMMMx.  *ML "M .M*  .MMMMMM**"
#                      *PMMMMMMhn. *x > M  .MMMM**""
#                        ""**MMMMhx/.h/ .=*"
#                                  .3P"%....
#                                nP"     "*MMnx                
#   __
#  |  ""--.--.._                                             __..    ,--.
#  |       `.   "-.'""\_...-----..._   ,--. .--..-----.._.""|   |   /   /
#  |_   _    \__   ).  \           _/_ |   \|  ||  ..    >  `.  |  /   /
#    | | `.   ._)  /|\  \ .-"""":-"   "-.   `  ||  |.'  ,'`. |  |_/_  /
#    | |_.'   |   / ""`  \  ===/  ..|..  \     ||      < ""  `.  "  |/__
#    `.      .    \ ,--   \-..-\   /"\   /     ||  |>   )--   |    /    |
#     |__..-'__||__\   |___\ __.:-.._..-'_|\___||____..-/  |__|--""____/
#                           _______________________
#                          /                      ,'
#                         /      ___            ,'
#                        /   _.-'  ,'        ,-'   /
#                       / ,-' ,--.'        ,'   .'/
#                      /.'     `.         '.  ,' /
#                     /      ,-'       ,"--','  /
#                          ,'        ,'  ,'    /
#                         ,-'      ,' .-'     /
#                      ,-'                   /
#                    ,:_____________________/