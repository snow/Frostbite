#!/usr/bin/bash

shopt -s nullglob

system_map_location="/boot/System.map-* /usr/lib/modules/*/System.map"

counter=0
for filename in ${system_map_location} ; do
    counter=$(( counter + 1 ))
done

if [ "$counter" -ge "1" ]; then
    echo "Deleting system.map files..."
fi

for filename in ${system_map_location} ; do
    if [ -f "${filename}" ]; then
        shred --force --zero -u "${filename}"
        echo "removed '${filename}'"
    fi
done

if [ "$counter" -ge "1" ]; then
    echo "Done. Success."
fi
