#!/usr/bin/bash

## Copyright (C) 2012 - 2021 ENCRYPTED SUPPORT LP <adrelanos@whonix.org>
## See the file COPYING for copying conditions.

set -e

sysfs_whitelist=1
cpuinfo_whitelist=1

## https://www.whonix.org/wiki/Security-misc#selinux
selinux=0


# BugFix: Enable extended pattern matching in Bash to support more advanced regex functionality
shopt -s extglob
# EndFix


shopt -s nullglob

## Allows for disabling the whitelist.
for i in /etc/hide-hardware-info.d/*.conf
do
  bash -n "${i}"
  source "${i}"
done

create_whitelist() {
  if [ "${1}" = "sysfs" ]; then
    whitelist_path="/sys"
  elif [ "${1}" = "cpuinfo" ]; then
    whitelist_path="/proc/cpuinfo"
  else
    echo "ERROR: ${1} is not a correct parameter."
    exit 1
  fi

  if grep -q "${1}" /etc/group; then
    ## Changing the permissions of /sys recursively
    ## causes errors as the permissions of /sys/kernel/debug
    ## and /sys/fs/cgroup cannot be changed.
    chgrp -fR "${1}" "${whitelist_path}" || true

    chmod o-rwx "${whitelist_path}"
  else
    echo "ERROR: The ${1} group does not exist, the ${1} whitelist was not created."
  fi
}

## sysfs and debugfs expose a lot of information
## that should not be accessible by an unprivileged
## user which includes hardware info, debug info and
## more. This restricts /sys, /proc/cpuinfo, /proc/bus
## and /proc/scsi to the root user only. This hides
## many hardware identifiers from ordinary users
## and increases security.
for i in /proc/cpuinfo /proc/bus /proc/scsi /sys
do
  if [ -e "${i}" ]; then
    if [ "${i}" = "/sys" ]; then
      ## Whitelist for /sys.
      if [ "${sysfs_whitelist}" = "1" ]; then
        create_whitelist sysfs
      else
        chmod og-rwx /sys
        echo "INFO: The sysfs whitelist is not enabled. Some things may not work properly."
      fi
    elif [ "${i}" = "/proc/cpuinfo" ]; then
      ## Whitelist for /proc/cpuinfo.
      if [ "${cpuinfo_whitelist}" = "1" ]; then
        create_whitelist cpuinfo
      else
        chmod og-rwx /proc/cpuinfo
        echo "INFO: The cpuinfo whitelist is not enabled. Some things may not work properly."
      fi
    else
      chmod og-rwx "${i}"
    fi
  else
    ## /proc/scsi doesn't exist on Debian so errors
    ## are expected here.
    if ! [ "${i}" = "/proc/scsi" ]; then
      echo "ERROR: ${i} could not be found."
    fi
  fi
done

harden_sysfs() {
    chmod -R o-rwx /sys     || /usr/bin/true

    # BugFix: Let the fixing begin...
    chmod o+rx /sys
    # EndFix

    # BugFix: Support using X11 and Wayland with a single VirtIO GPU, and any VirtIO Keyboard/s+Tablet/s
    chmod o+rx /sys/bus/
    chmod o+rx /sys/class/
    chmod o+rx /sys/class/drm/
    chmod o+rx /sys/class/input/
    chmod o+rx /sys/dev/
    chmod o+rx /sys/dev/char/
    chmod o+rx /sys/devices/
    chmod o+rx /sys/devices/pci0000:00/

    BASE_PCI_DIRECTORY="/sys/devices/pci0000:00"
    VIRTIO_GPU_REGEX="[[:digit:]]+:[[:alnum:]]+:[[:alnum:]]+.[[:digit:]]+/virtio[[:digit:]]+/drm"
    VIRTIO_GPU_PATH="$(find "${BASE_PCI_DIRECTORY}" -type d -regextype posix-extended -regex "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_REGEX}" -print0 -quit)"
    VIRTIO_GPU_BUSID="$(echo "${VIRTIO_GPU_PATH}" | cut --delimiter=/ --fields=5)"
    chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/
    chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/device
    chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/subsystem_device
    chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/subsystem_vendor
    chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/uevent
    chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/vendor
    chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/virtio+([[:digit:]])/
    chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/virtio+([[:digit:]])/drm/
    chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/virtio+([[:digit:]])/drm/card+([[:digit:]])/
    chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/virtio+([[:digit:]])/drm/card+([[:digit:]])/uevent
    chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/virtio+([[:digit:]])/drm/card+([[:digit:]])/card+([[:digit:]])-Virtual-+([[:digit:]])/
    chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/virtio+([[:digit:]])/drm/card+([[:digit:]])/card+([[:digit:]])-Virtual-+([[:digit:]])/uevent
    chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_GPU_BUSID}"/virtio+([[:digit:]])/drm/renderD+([[:digit:]])/

    VIRTIO_PCIHIDS_REGEX="[[:digit:]]+:[[:alnum:]]+:[[:alnum:]]+.[[:digit:]]+/[[:digit:]]+:[[:digit:]]+:[[:digit:]]+.[[:digit:]]+/virtio[[:digit:]]+/input"
    readarray -d '' VIRTIO_PCIHID_PATHS < <(find "${BASE_PCI_DIRECTORY}" -type d -regextype posix-extended -regex "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHIDS_REGEX}" -print0)
    for VIRTIO_PCIHID in "${VIRTIO_PCIHID_PATHS[@]}"; do
        VIRTIO_PCIHID_BUSID_0="$(echo "${VIRTIO_PCIHID}" | cut --delimiter=/ --fields=5)"
        VIRTIO_PCIHID_BUSID_1="$(echo "${VIRTIO_PCIHID}" | cut --delimiter=/ --fields=6)"
        chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHID_BUSID_0}"/
        chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHID_BUSID_0}"/"${VIRTIO_PCIHID_BUSID_1}"/
        chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHID_BUSID_0}"/"${VIRTIO_PCIHID_BUSID_1}"/virtio+([[:digit:]])/
        chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHID_BUSID_0}"/"${VIRTIO_PCIHID_BUSID_1}"/virtio+([[:digit:]])/input/
        chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHID_BUSID_0}"/"${VIRTIO_PCIHID_BUSID_1}"/virtio+([[:digit:]])/input/input+([[:digit:]])/
        chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHID_BUSID_0}"/"${VIRTIO_PCIHID_BUSID_1}"/virtio+([[:digit:]])/input/input+([[:digit:]])/uevent
        chmod o+rx "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHID_BUSID_0}"/"${VIRTIO_PCIHID_BUSID_1}"/virtio+([[:digit:]])/input/input+([[:digit:]])/event+([[:digit:]])/
        chmod o+r       "${BASE_PCI_DIRECTORY}"/"${VIRTIO_PCIHID_BUSID_0}"/"${VIRTIO_PCIHID_BUSID_1}"/virtio+([[:digit:]])/input/input+([[:digit:]])/event+([[:digit:]])/uevent
    done
    # EndFix

    # BugFix: Support using Firefox with X11, solves the "No GPUs detected via PCI" error message
    chmod o+rx /sys/bus/pci/
    chmod o+rx /sys/bus/pci/devices/

    chmod o+rx /sys/devices/pci0000:00/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/
    chmod o+r       /sys/devices/pci0000:00/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/{class,device,vendor}
    chmod o+rx /sys/devices/pci0000:00/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/
    chmod o+r       /sys/devices/pci0000:00/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/{class,device,vendor}
    # EndFix

    # BugFix: Support using KLOAK with Wayland, and X11
    chmod o+rx /sys/devices/virtual/
    chmod o+rx /sys/devices/virtual/input/
    # EndFix
}

## https://www.whonix.org/wiki/Security-misc#selinux
##
## on SELinux systems, at least /sys/fs/selinux
## must be visible to unprivileged users, else
## SELinux userspace utilities will not function
## properly
if [ -d /sys/fs/selinux ]; then
    if [ "${selinux}" = "1" ]; then
        harden_sysfs

        # BugFix: Support SELinux on Fedora, this simply restores the default permissions
        chmod o+rx /sys/fs /sys/fs/selinux

        find /sys/fs/selinux -type d -exec chmod o+rx "{}" \;

        find /sys/fs/selinux/avc/                   -type f -exec chmod o+r "{}" \;
        find /sys/fs/selinux/booleans/              -type f -exec chmod o+r "{}" \;
        find /sys/fs/selinux/class/                 -type f -exec chmod o+r "{}" \;
        find /sys/fs/selinux/initial_contexts/      -type f -exec chmod o+r "{}" \;
        find /sys/fs/selinux/policy_capabilities/   -type f -exec chmod o+r "{}" \;
        find /sys/fs/selinux/ss/                    -type f -exec chmod o+r "{}" \;

        chmod o+rw /sys/fs/selinux/access
        chmod o+r  /sys/fs/selinux/checkreqprot
        chmod o+rw /sys/fs/selinux/context
        chmod o+rw /sys/fs/selinux/create
        chmod o+r  /sys/fs/selinux/deny_unknown
        chmod o+r  /sys/fs/selinux/enforce
        chmod o+rw /sys/fs/selinux/member
        chmod o+r  /sys/fs/selinux/mls
        chmod o+rw /sys/fs/selinux/null
        chmod o+r  /sys/fs/selinux/policy
        chmod o+r  /sys/fs/selinux/policyvers
        chmod o+r  /sys/fs/selinux/reject_unknown
        chmod o+rw /sys/fs/selinux/relabel
        chmod o+r  /sys/fs/selinux/status
        chmod o+rw /sys/fs/selinux/user
        chmod o+w  /sys/fs/selinux/validatetrans
        # EndFix

        echo "INFO: SELinux mode enabled. Restrictions loosened slightly in order to allow userspace utilities to function."
    else
        harden_sysfs
        echo "INFO: SELinux detected, but SELinux mode is not enabled. Some userspace utilities may not work properly."
    fi
else
    harden_sysfs
fi
