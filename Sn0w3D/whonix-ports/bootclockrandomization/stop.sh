#!/usr/bin/bash

set -e

source /usr/lib/whonix-ports/bootclockrandomization/shared.sh

do_stop() {
   if [ -e "$FAIL_FILE" ]; then
      rm -f "$FAIL_FILE"
   fi
   if [ -e "$SUCCESS_FILE" ]; then
      rm -f "$SUCCESS_FILE"
   fi
}

do_stop
