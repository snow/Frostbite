#!/bin/bash


# Exits if the script isn't running as root.
if [[ "$(id -u)" -ne 0 ]]; then
    echo "[!] Run Me As Root [!]"
    exit 1
fi

# Select a random website out of the pool.
select_pool() {
  # Keybase.
  POOL[1]="http://keybase5wmilwokqirssclfnsqrjdsi7jdir5wy7y7iu3tanwmtp6oid.onion"
  
  # Wikileaks.
  POOL[2]="http://rpzgejae7cxxst5vysqsijblti4duzn3kjsmn43ddi2l3jblhk4a44id.onion"

  # GetMonero.
  POOL[3]="http://monerotoruzizulg5ttgat2emf4d6fbmiea25detrmmy7erypseyteyd.onion"

  # DuckDuckGo.
  POOL[4]="https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion"

  # The Guardian (.onion = securedrop).
  POOL[5]="http://xp44cagis447k3lpb4wwhcqukix6cgqokbuys24vmxmbzmaq2gjvc2yd.onion"

  # The last one doesn't get selected. Without the following line, POOL[5] would never be selected.
  POOL[6]=""

  rand=$[$RANDOM % ${#POOL[@]}]
  SELECTED_POOL="${POOL[$rand]}"

  # If nothing was selected, run select_pool again.
  if [ "${SELECTED_POOL}" = "" ]; then
    select_pool
  fi
}

select_pool

# Configure curl to use a socks proxy at localhost on port 9050. This is the default Tor socksport.
SECURE_CURL="/usr/bin/curl -sI --socks5-hostname 127.0.0.1:9050"

if ! ${SECURE_CURL} -s ${SELECTED_POOL} &>/dev/null; then
  echo "ERROR 404"
  exit 1
fi

# Extract the current time from the http header when connecting to one of the websites in the pool.
NEW_TIME=$(${SECURE_CURL} ${SELECTED_POOL} 2>&1 | grep -i "Date" | sed -e 's/Date: //' | sed -e 's/date: //')

# Output the extracted time and selected pool for debugging.
if [ "${DEBUG_TS}" = "1" ]; then
  echo "${SELECTED_POOL}"
  echo "${NEW_TIME}"
fi

# Set the time to the value we just extracted.
date -s "${NEW_TIME}"
