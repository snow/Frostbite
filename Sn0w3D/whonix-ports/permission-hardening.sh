#!/usr/bin/bash

set -e
set -o pipefail

set_file_perms() {
    echo "INFO: START parsing config_file: ${config_file}"
    /usr/bin/bash   "${config_file}"
    echo "INFO: END parsing config_file: ${config_file}"
}

parse_config_folder() {
    shopt -s nullglob
    for config_file in /etc/permission-hardening.d/*.conf; do
        set_file_perms
    done
}

parse_config_folder
