#!/usr/bin/bash

set -x

case "${PAM_SERVICE}" in
    doas)
        exit 0
        ;;
    login)
        exit 0
        ;;
    su)
        exit 1
        ;;
    su-l)
        exit 0
        ;;
    *)
        exit 1
        ;;
esac
